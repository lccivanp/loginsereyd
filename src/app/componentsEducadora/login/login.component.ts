import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Router } from '@angular/router'
import { FacebookService, InitParams, LoginResponse, LoginOptions } from 'ngx-facebook';
import { AuthAdminService } from '../../servicios/auth-admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService,
    private authAdmin: AuthAdminService,
    private router: Router,
    private fb: FacebookService) {

    let initParams: InitParams = {
      appId: '220498225070597',
      xfbml: true,
      version: 'v2.8'
    };

    fb.init(initParams);
  }

  ngOnInit() {
    //revisar si es admi
    if (this.authAdmin.isLoggedIn){
      this.router.navigate(['admin'])
    }else
    if (this.auth.isLoggedIn) {
      this.router.navigate(['dashboard'])

      //console.log(this.auth.userId)
      //this.message = "Hola "+this.auth.userId.name + " !!";
    } else {
      this.router.navigate([''])
    }
  }

  loginWithFacebook(event): void {
    event.preventDefault();
    // login with options
    const options: LoginOptions = {
      scope: 'public_profile,user_friends,email,pages_show_list',
      return_scopes: true,
      enable_profile_selector: true
    };
    this.fb.login(options)
      .then((response: LoginResponse) => {
        console.log(response)
        this.auth.getUserDetailsFacebook(response.authResponse.userID).subscribe(
          (val) => {
            this.router.navigate(['dashboard'])
            this.auth.setLoggedIn(true)
            this.auth.setIdUser(val.user)
          },
          err => {
            window.alert("No se encontraro usuario")
          },
          () => {
            //console.log("The POST observable is now completed.");
          })

      })
      .catch((error: any) => {

        window.alert("No se encontraro usuario")
      });

  }

  loginUser(event) {
    event.preventDefault();
    const target = event.target
    const username = target.querySelector('#username').value
    const password = target.querySelector('#password').value
    this.auth.getUserDetails(username, password).subscribe(
      (val) => {
        //console.log("POST call successful value returned in body", val.user);
        this.router.navigate(['dashboard'])
        this.auth.setLoggedIn(true)
        this.auth.setIdUser(val.user)
      },
      err => {
        console.log("POST call in error", err.description);
        window.alert("No se encontraro usuario")
      },
      () => {
        console.log("The POST observable is now completed.");
      })

  }

}

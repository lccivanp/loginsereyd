import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Users } from '../../modelos/users';
import { DataService } from '../../servicios/data.service';
import { Groups } from '../../modelos/groups';
import { studentScholar } from '../../modelos/studentScholar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  message = "Loading ..."
  myAuthUserAdmin: Users
  groupSelect: Groups
  studentScholar: studentScholar[]

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private data: DataService
  ) { }

  ngOnInit() {
  
    
    if (this.auth.isLoggedIn) {
      
      this.router.navigate([{outlets: {content: 'groups'}}]);
      
      this.message = "Hola " + this.auth.userId.name + " !!";
      this.myAuthUserAdmin = this.auth.userId
      
      
      this.data.currentGroup.subscribe(
        groupSelect => {
          if (groupSelect !== undefined)
            this.groupSelect = groupSelect
          console.log("Grupo "+this.groupSelect)
        },
        error => {
          console.error(error);
  
        }
      )
  
      this.data.currentStudentScholar.subscribe(
        studentsSource => this.studentScholar = studentsSource
      )

    } else {
      this.router.navigate(['nothing'])
    }

    

    
  }

  setStatusBarIndex(index: number, value: number) {
    let progressBar = ''
    switch (value) {
      case 1: {
        //statements; 
        progressBar = '' + this.studentScholar[index].language
        break;
      }
      case 1: {
        //statements; 
        progressBar = '' + this.studentScholar[index].math
        break;
      } case 1: {
        //statements; 
        progressBar = '' + this.studentScholar[index].world
        break;
      }
      case 1: {
        //statements; 
        progressBar = '' + this.studentScholar[index].health
        break;
      }
      case 1: {
        //statements; 
        progressBar = '' + this.studentScholar[index].social
        break;
      }
      case 1: {
        //statements; 
        progressBar = '' + this.studentScholar[index].art
        break;
      }
    }

    return "'width':"+ progressBar + "%"

  }

  setStatusBar(value: number):Object{
    let status = value + '%'
    let styles ={
      'width' : status
    }
    return styles
  }

  countBoys():number{
    let count = 0;
    this.groupSelect.students.forEach((itemS,indexS)=>{
      if(itemS.gender == "1")
        count++;
    })
    
    
    return count;
  }

  countGirls():number{
    let count = 0;
    this.groupSelect.students.forEach((itemS,indexS)=>{
      if(itemS.gender != "1")
        count++;
    })
   
    return count;
  }

  calculateGroupBehaviorByDes():String{
    let gruopBe = 0
    this.studentScholar.forEach((itemS,indexS)=>{
      switch(itemS.behavior.description){
        case 'Buena':
          gruopBe +=2
          break
        case 'Regular':
          gruopBe +=1
          break
        case 'Malo':
          gruopBe +=0
          break
      }
    })
    let avg = gruopBe / this.studentScholar.length
    let avgValue = ''
    if( avg >= 0 && avg <=1){
      avgValue = 'Mala'
    }else if (avg > 1 && avg < 1.5){
      avgValue = 'Regular'
    }else if  (avg > 1.5 && avg <=2){
      avgValue = 'Buena'
    }
    return avgValue
  }


}

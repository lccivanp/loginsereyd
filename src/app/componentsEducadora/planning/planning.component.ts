import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../../servicios/data.service';
import { Users } from '../../modelos/users';
import { Groups } from '../../modelos/groups';
import { studentScholar } from '../../modelos/studentScholar';
import { PlaceholderMapper } from '@angular/compiler/src/i18n/serializers/serializer';
import { Planification } from '../../modelos/planification';
import { PlanificationService } from '../../servicios/planification.service';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit {

  message = "Loading ..."
  myAuthUserAdmin: Users
  planifications: Planification[] =[]
  
  constructor( 
    private auth: AuthService,
    private planificationService : PlanificationService,
    private router: Router,
    private data: DataService) { }

  ngOnInit() {
    if (this.auth.isLoggedIn) {
      this.message = "Hola " + this.auth.userId.name + " !!";
      this.myAuthUserAdmin = this.auth.userId
      
      this.planificationService.getPlanificationsByUser(this.auth.userId.id_user).subscribe(
        (val) => {
          //console.log(val)
          this.planifications = val.planifications
         
        
        },
        err => {
           
        },
        () => {
            
        }
      )

    } else {
      this.router.navigate([''])
    }
    //console.log(this.planifications);
    
  }

  descargarArchivo(event,url) {
    this.planificationService.downloadPDF(url).subscribe(
      (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
      }
    );

  }

}

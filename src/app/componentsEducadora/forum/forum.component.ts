import { Component, OnInit } from '@angular/core';
import { DiscussionService } from '../../servicios/discussion.service';
import { AuthService } from '../../servicios/auth.service';
import { Router } from '@angular/router';
import { Discussion } from '../../modelos/discussion';
import { Users } from '../../modelos/users';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.css']
})
export class ForumComponent implements OnInit {
  myAuthUserAdmin: Users
  discussions: Discussion[] =[]

  constructor(
    private auth: AuthService,
    private discussionService : DiscussionService,
    private router: Router) { }

  ngOnInit() {

    if (this.auth.isLoggedIn) {
     
      this.myAuthUserAdmin = this.auth.userId
      
      this.discussionService.getDiscussionByUser(this.auth.userId.id_user).subscribe(
        (val) => {
          //console.log(val)
          this.discussions = val.questions
         
        
        },
        err => {
           
        },
        () => {
            
        }
      )

    } else {
      this.router.navigate([''])
    }
    //console.log(this.planifications);
  }

}

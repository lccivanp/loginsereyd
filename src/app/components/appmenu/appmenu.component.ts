import { Component, OnInit, Input } from '@angular/core';
import { Users } from '../../modelos/users';
import { AuthService } from '../../servicios/auth.service';
import { Group } from '../../modelos/group';
import { GroupService } from '../../servicios/group.service';
import { Cooperation } from '../../modelos/cooperation';
import { Homework } from '../../modelos/homework';
import { Notice } from '../../modelos/notice';
import { Student } from '../../modelos/student';
import { Groups } from '../../modelos/groups';
import { DataService } from '../../servicios/data.service';
import { studentScholar } from '../../modelos/studentScholar';
import { StudentScholarService } from '../../servicios/student-scholar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appmenu',
  templateUrl: './appmenu.component.html',
  styleUrls: ['./appmenu.component.css']
})
export class AppmenuComponent implements OnInit {

  @Input() myAuthUser: Users;
  infoUsuario: Groups[] = [];
  groupSelect: Groups;
  students: studentScholar[];


  constructor(
    private auth: AuthService,
    private groups: GroupService,
    private data: DataService,
    private router: Router,
    private studentService: StudentScholarService
  ) { }

  ngOnInit() {
    this.students = []
    if (this.auth.isLoggedIn) {

      this.groups.getGroups(this.myAuthUser.id_user).subscribe(
        (val) => {
          val.data.forEach((item, index) => {

            this.infoUsuario[index] = item
            if (index == 0) {
              this.infoUsuario[index].active = "active"
              this.data.setGroupSelect(item)
              this.infoUsuario[index].students.forEach((itemS, indexS) => {

                this.studentService.getInfoStudentScholar(itemS.id_student).subscribe(
                  (val) => {
                    this.students[indexS] = val

                  },
                  err => {

                  }
                );
              })

            } else {
              this.infoUsuario[index].active = ""
            }
          });
        },
        err => {
          //console.log("GET call in error", err.description);
          //window.alert("No se encontraro usuario")
        },
        () => {

        }
      )
      this.data.setStudentSource(this.students)
      this.data.currentGroup.subscribe(
        groupSelect => this.groupSelect = groupSelect
      )
      this.data.currentStudentScholar.subscribe(
        studentsSource => this.students = studentsSource
      )
    }


  }

  getGroupById(id) {
    this.students = []
    this.infoUsuario.forEach((item, index) => {
      //console.log(item)
      if (item.group.id_group == id) {
        this.data.setGroupSelect(item)
        item.students.forEach((itemS, indexS) => {
          //console.log("buscara el id del studiante" + itemS.id_student)
          this.studentService.getInfoStudentScholar(itemS.id_student).subscribe(
            (val) => {
              this.students[indexS] = val
            },
            err => {

            }
          );
        })
        this.data.setStudentSource(this.students)
      }
    })


  }

  cleanActive(id_gruop) {
    this.infoUsuario.forEach((item, index) => {
      //console.log(item)

      if (item.group.id_group == id_gruop) {
        item.active = "active"
      }
    })
  }

  changeGroup(event, id_gruop) {
    event.preventDefault();
    let click_id = event.target.id
    console.log(this.router.url)
    if (this.router.url !== "/admin(content:groups)")
      this.router.navigate([{outlets: {content: 'groups'}}])
      //this.router.navigate(['/admin'])
    this.getGroupById(id_gruop)


  }

  changePlanning(event) {
    event.preventDefault();
    if (this.router.url !== "/admin(content:groups")
      this.router.navigate([{outlets: {content: 'planning'}}])
  }

  cambioPagina(event) {
    event.preventDefault();
    if (this.router.url !== "/admin")
      this.router.navigate(['admin'])



  }


}

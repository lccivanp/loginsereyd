import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Users } from '../../modelos/users';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppheaderComponent implements OnInit {

  myUser : Users
  @Input() myAuthUser: Users;

  constructor(private Auth : AuthService,
    private router: Router,
  ) { }
  

  ngOnInit() {
    
    this.myUser = this.myAuthUser
   
    
    
  }


  logout(event){
    this.router.navigate([{outlets: {primary:"logout",content: 'nothing'}}])
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Pipe } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './componentsEducadora/login/login.component';
import { DashboardComponent } from './componentsEducadora/dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { AuthService } from './servicios/auth.service';
import { AuthGuard } from './auth.guard'
import { FacebookModule } from 'ngx-facebook';
import { LogoutComponent } from './componentsEducadora/logout/logout.component';
import { AppheaderComponent } from './components/appheader/appheader.component';
import { AppfooterComponent } from './components/appfooter/appfooter.component';
import { AppmenuComponent } from './components/appmenu/appmenu.component';
import { AppsettingsComponent } from './components/appsettings/appsettings.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';

import { HashLocationStrategy, LocationStrategy } from '@angular/common'
import { GroupService } from './servicios/group.service';
import { DataService } from './servicios/data.service'
import { imageServer } from './common/pipes';
import { StudentScholarService } from './servicios/student-scholar.service';
import { PlanningComponent } from './componentsEducadora/planning/planning.component';

import { PlanificationService } from './servicios/planification.service';
import { GroupsComponent } from './componentsEducadora/groups/groups.component';
import { NothingComponent } from './nothing/nothing.component';
import { AdminComponent } from './components-admin/admin/admin.component';
import { LoginAdminComponent } from './components-admin/login-admin/login-admin.component';
import { LogoutAdminComponent } from './components-admin/logout-admin/logout-admin.component';
import { PlanningAdminComponent } from './components-admin/planning-admin/planning-admin.component';
import { AuthAdminService } from './servicios/auth-admin.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlanningLoadComponent } from './components-admin/planning-load/planning-load.component';
import { ResourceAdminComponent } from './components-admin/resource-admin/resource-admin.component';
import { ResourceLoadComponent } from './components-admin/resource-load/resource-load.component';
import { ResourcesService } from './servicios/resources.service';
import { ResourceCategoryComponent } from './components-admin/resource-category/resource-category.component';
import { ForumComponent } from './componentsEducadora/forum/forum.component';
import { EducSearchComponent } from './components-admin/educ-search/educ-search.component';



const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children :[
      {
        path: 'loadplanning',
        component: PlanningLoadComponent,
        //canActivate :[AuthGuard]
      },
      {
        path: 'planning',
        component: PlanningAdminComponent,
        //canActivate :[AuthGuard]
      },
      {
        path: 'resource',
        component: ResourceAdminComponent,
        //canActivate :[AuthGuard]
      },
      {
        path: 'loadresource',
        component: ResourceLoadComponent,
        //canActivate :[AuthGuard]
      },
      {
        path: 'categoryresource',
        component: ResourceCategoryComponent,
        //canActivate :[AuthGuard]
      },
      {
        path: 'search',
        component: EducSearchComponent,
        //canActivate :[AuthGuard]
      },
      
      
      
    ]
    //canActivate :[AuthGuard]
  },
  {
    path: 'loginAdmin',
    component: LoginAdminComponent,
    //canActivate :[AuthGuard]
  },
  {
    path: 'logoutAdmin',
    component: LogoutAdminComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    component: PagenotfoundComponent
  },
  {
    path: "groups",
    component: GroupsComponent,
    outlet: "content"
  },
  {
    path: "planning",
    component: PlanningComponent,
    outlet: "content"
  },
  {
    path: 'nothing',
    component: NothingComponent,
    outlet: "content",
    pathMatch: 'full'
  }
  

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HomeComponent,
    LogoutComponent,
    AppheaderComponent,
    AppfooterComponent,
    AppmenuComponent,
    AppsettingsComponent,
    PagenotfoundComponent,
    imageServer,
    PlanningComponent,
    GroupsComponent,
    NothingComponent,
    AdminComponent,
    LoginAdminComponent,
    LogoutAdminComponent,
    PlanningAdminComponent,
    PlanningLoadComponent,
    ResourceAdminComponent,
    ResourceLoadComponent,
    ResourceCategoryComponent,
    ForumComponent,
    EducSearchComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routes,{ useHash: true }),
    FacebookModule.forRoot()
  ],
  providers: [AuthService, AuthGuard, GroupService, DataService, StudentScholarService, PlanificationService,ResourcesService ,AuthAdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './servicios/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  
  constructor(private auth: AuthService, private router: Router){
    
  }
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      console.log("Estoy loogeado"+this.auth.isLoggedIn)
      if(!this.auth.isLoggedIn){   
        //console.log("Se esta redireccionando")
        this.router.navigate([''])
      }
    return this.auth.isLoggedIn;
  }
}

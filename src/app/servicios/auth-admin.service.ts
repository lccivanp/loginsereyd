import { Injectable } from '@angular/core';
import { UserAdmin } from '../modelos/userAdmin';
import {HttpClient} from '@angular/common/http'
import {Observable} from "rxjs/Observable"
import { HttpParams } from '@angular/common/http';


interface miRespuesta {
  error: String,
  description : String,
  user?: UserAdmin

}

@Injectable()
export class AuthAdminService {
  userAdmin : UserAdmin = JSON.parse(localStorage.getItem('userAdmin') )
  loggedInStatus = JSON.parse(localStorage.getItem('loggedInAdmin') || 'false' )


  constructor(private _http: HttpClient) { }

  setLoggedIn(value : boolean){
    this.loggedInStatus = value
    localStorage.setItem('loggedInAdmin',''+value)
  }

  get isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedInAdmin') || this.loggedInStatus.toString() )
  }

  setIdUser(value: UserAdmin){
    this.userAdmin = value
  
    localStorage.setItem('userAdmin',JSON.stringify(value))
  }

  get userId() : UserAdmin{
    return JSON.parse(localStorage.getItem('userAdmin')  )
  }

  getUserDetails(email , password) : Observable<miRespuesta>{
    //post these details to API
    
    const serviceURL = "https://app.soyeducadorasereyd.com/loginAdmin/"
    //return this._http.get(serviceURL).map(response => response.json());
    const params = new HttpParams()  
        .set('email', email)
        .set('password', password);
  
   //this._http.post(serviceURL,{ params}).map(response => response.json())
   return this._http.post<miRespuesta>(serviceURL,{
      'email':email,
      'password':password
    })
  
    
  }

  deleteSessionUser() {
    this.setLoggedIn(false)
    localStorage.removeItem("userAdmin")
    localStorage.removeItem("loggedInAdmin")
  
}


}

import { TestBed, inject } from '@angular/core/testing';

import { EducadoraService } from './educadora.service';

describe('EducadoraService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EducadoraService]
    });
  });

  it('should be created', inject([EducadoraService], (service: EducadoraService) => {
    expect(service).toBeTruthy();
  }));
});

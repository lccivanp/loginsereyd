import { TestBed, inject } from '@angular/core/testing';

import { StudentScholarService } from './student-scholar.service';

describe('StudentScholarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentScholarService]
    });
  });

  it('should be created', inject([StudentScholarService], (service: StudentScholarService) => {
    expect(service).toBeTruthy();
  }));
});

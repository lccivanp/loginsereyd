import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Users } from '../modelos/users';
import { Groups } from '../modelos/groups';
import { studentScholar } from '../modelos/studentScholar';

@Injectable()
export class DataService {

  private messageSource = new BehaviorSubject<string>("default message");  
  currentMessage = this.messageSource.asObservable();

  private groupSelect: BehaviorSubject<Groups> = new BehaviorSubject<Groups>(undefined);
  currentGroup = this.groupSelect.asObservable();

  private studentsSource = new BehaviorSubject<studentScholar[]>(undefined)
  currentStudentScholar = this.studentsSource.asObservable();
  

  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  setGroupSelect(groupSelect:Groups){
    this.groupSelect.next( groupSelect);
    
  }

  setStudentSource(studentsSource: studentScholar[]){
    this.studentsSource.next(studentsSource)
  }

}
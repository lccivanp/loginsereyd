import { Injectable } from '@angular/core';
import { Resource } from '../modelos/resource';
import { HttpParams, HttpClient } from "@angular/common/http";
import{ ResponseContentType, Http } from '@angular/http';
import { Observable } from "rxjs/Observable"


interface miRespuesta {
  error: String,
  resources: Resource[]

}

interface miRespuestaCategorias {
  error: Number,
  description : string,
  resources: {
    name:string,
    id_resource:string
  }[]

}

@Injectable()
export class ResourcesService {

  constructor(private _http: HttpClient,
    private _http2: Http) { }

    getResourceCategory(): Observable<miRespuestaCategorias> {
      const serviceURL = "https://app.soyeducadorasereyd.com/resourceAdmin"
      return this._http.get<miRespuestaCategorias>(serviceURL);
    }
  
    getResourceAdmin(): Observable<miRespuestaCategorias> {
      const serviceURL = "https://app.soyeducadorasereyd.com/planificationAdmin"
      return this._http.get<miRespuestaCategorias>(serviceURL);
    }

    getResourceByCategory(idResource): Observable<miRespuesta> {
      const serviceURL = "https://app.soyeducadorasereyd.com/resource/" + idResource
      console.log(serviceURL);
      return this._http.get<miRespuesta>(serviceURL);
    }

    getResourceByUser(idUser): Observable<miRespuesta> {

      const serviceURL = "https://app.soyeducadorasereyd.com/resource/user/" + idUser
      console.log(serviceURL);
      return this._http.get<miRespuesta>(serviceURL);
    }
  
  
    uploadResource(model) {
      const serviceURL = "https://app.soyeducadorasereyd.com/resource/"
      return this._http.post<miRespuesta>(serviceURL, model)
  
    }
  
    updateResource(resource : Resource){
      const serviceURL = "https://app.soyeducadorasereyd.com/resource/"
      return this._http.put<miRespuesta>(serviceURL,resource)
    }

    uploadResourceCategory(model) {
      const serviceURL = "https://app.soyeducadorasereyd.com/resourceCategory/"
      return this._http.post<miRespuesta>(serviceURL, model)
  
    }

    removeResource(resource : Resource){
      resource.active = "2"
      const serviceURL = "https://app.soyeducadorasereyd.com/resource/"
      return this._http.put<miRespuesta>(serviceURL,resource)
    }

    updateResourceCategory(resourceCategory : any){
      const serviceURL = "https://app.soyeducadorasereyd.com/resourceCategory/"
      return this._http.put<miRespuesta>(serviceURL,resourceCategory)
    }

    removeResourceCategory(resourceCategory : any){
      resourceCategory.active = "2"
      const serviceURL = "https://app.soyeducadorasereyd.com/resourceCategory/"
      return this._http.put<miRespuesta>(serviceURL,resourceCategory)
    }
  
    
    downloadPDF(fileS): any {
      const serviceURL  = "https://app.soyeducadorasereyd.com/"+ fileS
      return this._http2.get(serviceURL, 
        { responseType: ResponseContentType.Blob })
        .map(
      (res) => {
              return new Blob([res.blob()], { type: 'application/pdf' })
          }
        )
      }
}

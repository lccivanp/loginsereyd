import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Http, Headers} from '@angular/http'
import {Observable} from "rxjs/Observable"
import "rxjs/add/operator/map"
import  {Users} from "./../modelos/users"
import {HttpParams} from "@angular/common/http";

interface miRespuesta {
  error: String,
  description : String,
  user?: Users

}
@Injectable()
export class AuthService {

  user : Users = JSON.parse(localStorage.getItem('user') )
  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false' )

  constructor(private _http: HttpClient){ }

  setLoggedIn(value : boolean){
    this.loggedInStatus = value
    localStorage.setItem('loggedIn',''+value)
  }

  get isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString() )
  }
  
  setIdUser(value: Users){
    this.user = value
    var imagen = value.image
    var expr = /cloudinary/  // no quotes here
    if(!expr.test(imagen)){
      value.image = "https://app.soyeducadorasereyd.com" + value.image
    }
    localStorage.setItem('user',JSON.stringify(value))
  }

  get userId() : Users{
    return JSON.parse(localStorage.getItem('user')  )
  }

  getUserDetails(email , password) : Observable<miRespuesta>{
    //post these details to API
    
    const serviceURL = "https://app.soyeducadorasereyd.com/login"
    //return this._http.get(serviceURL).map(response => response.json());
    const params = new HttpParams()  
        .set('email', email)
        .set('password', password);
  
   //this._http.post(serviceURL,{ params}).map(response => response.json())
   return this._http.post<miRespuesta>(serviceURL,{
      'email':email,
      'password':password
    })
  
    
  }

  getUserDetailsFacebook(id_facebook) : Observable<miRespuesta>{
    //post these details to API
    
    const serviceURL = "https://app.soyeducadorasereyd.com/login/facebook"
    //return this._http.get(serviceURL).map(response => response.json());
    const params = new HttpParams()  
        .set('id_facebook"', id_facebook)
  
   //this._http.post(serviceURL,{ params}).map(response => response.json())
   return this._http.post<miRespuesta>(serviceURL,{
    'id_facebook':id_facebook
  })
  
    
  }
  
  deleteSessionUser() {
      this.setLoggedIn(false)
      localStorage.removeItem("user")
      localStorage.removeItem("loggedIn")
    
  }

}

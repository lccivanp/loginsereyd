import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Observable} from "rxjs/Observable"
import { studentScholar } from '../modelos/studentScholar';



@Injectable()
export class StudentScholarService {

  constructor(private _http: HttpClient) { }

  getInfoStudentScholar(idStudent):Observable<studentScholar>{
    const serviceURL = "https://app.soyeducadorasereyd.com/student/information/"+idStudent
    return this._http.get<studentScholar>(serviceURL);
  }

  getInfoStudent(idStudent):Observable<studentScholar>{
    const serviceURL = "https://app.soyeducadorasereyd.com/student/"+idStudent
    return this._http.get<studentScholar>(serviceURL);
  }


}






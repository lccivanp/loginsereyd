import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Http, Headers} from '@angular/http'
import {Observable} from "rxjs/Observable"
import "rxjs/add/operator/map"
import  { Users } from "./../modelos/users"
import  { Group } from "./../modelos/group"
import {HttpParams} from "@angular/common/http";

import { Groups } from '../modelos/groups';

interface miRespuesta {
  error: String,
  data :Groups[]

}

@Injectable()
export class GroupService {

  constructor(private _http: HttpClient) { }

  getGroups(idUser):Observable<miRespuesta>{
    const serviceURL = "https://app.soyeducadorasereyd.com/group/user/"+idUser
    return this._http.get<miRespuesta>(serviceURL);
  }

  getInfoGroup(idGroup):Observable<miRespuesta>{
    const serviceURL = "https://app.soyeducadorasereyd.com/group/"+idGroup
    return this._http.get<miRespuesta>(serviceURL);
  }



}

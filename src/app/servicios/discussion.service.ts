import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/Observable"
import { Discussion } from '../modelos/discussion';

interface miRespuesta {
  error: String,
  questions :Discussion[]

}


@Injectable()
export class DiscussionService {

  constructor(private _http: HttpClient) { }

  getDiscussionByUser(idUser):Observable<miRespuesta>{
    const serviceURL = "https://app.soyeducadorasereyd.com/discussion_user/"+idUser
    return this._http.get<miRespuesta>(serviceURL);
  }

 

}

import { Injectable } from '@angular/core';
import { Planification } from '../modelos/planification';
import { HttpParams, HttpClient } from "@angular/common/http";
import{ ResponseContentType, Http } from '@angular/http';
import { Observable } from "rxjs/Observable"


interface miRespuesta {
  error: String,
  planifications: Planification[]

}
@Injectable()
export class PlanificationService {



  constructor(private _http: HttpClient,
    private _http2: Http) { }

  getPlanifications(): Observable<miRespuesta> {
    const serviceURL = "https://app.soyeducadorasereyd.com/planification"
    return this._http.get<miRespuesta>(serviceURL);
  }

  getPlanificationsAdmin(): Observable<miRespuesta> {
    const serviceURL = "https://app.soyeducadorasereyd.com/planificationAdmin"
    return this._http.get<miRespuesta>(serviceURL);
  }

  getPlanificationsByUser(idUser): Observable<miRespuesta> {

    const serviceURL = "https://app.soyeducadorasereyd.com/planification/user/" + idUser
    console.log(serviceURL);

    return this._http.get<miRespuesta>(serviceURL);
  }


  uploadPlanification(model) {
    const serviceURL = "https://app.soyeducadorasereyd.com/planification/"
    return this._http.post<miRespuesta>(serviceURL, model)

  }

  updatePlanification(planification : Planification){
    const serviceURL = "https://app.soyeducadorasereyd.com/planification/"
    return this._http.put<miRespuesta>(serviceURL,planification)
  }

  removePlanification(planification : Planification){
    planification.active = "2"
    const serviceURL = "https://app.soyeducadorasereyd.com/planificationRemove/"
    return this._http.put<miRespuesta>(serviceURL,planification)
  }

  
  downloadPDF(fileS): any {
    const serviceURL  = "https://app.soyeducadorasereyd.com/"+ fileS
    return this._http2.get(serviceURL, 
      { responseType: ResponseContentType.Blob })
      .map(
    (res) => {
            return new Blob([res.blob()], { type: 'application/pdf' })
        }
      )
    }
}

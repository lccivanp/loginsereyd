import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducSearchComponent } from './educ-search.component';

describe('EducSearchComponent', () => {
  let component: EducSearchComponent;
  let fixture: ComponentFixture<EducSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

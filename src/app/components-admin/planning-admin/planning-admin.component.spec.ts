import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningAdminComponent } from './planning-admin.component';

describe('PlanningAdminComponent', () => {
  let component: PlanningAdminComponent;
  let fixture: ComponentFixture<PlanningAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanningAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

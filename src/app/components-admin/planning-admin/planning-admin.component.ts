import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PlanificationService } from '../../servicios/planification.service';
import { Planification } from '../../modelos/planification';

@Component({
  selector: 'app-planning-admin',
  templateUrl: './planning-admin.component.html',
  styleUrls: ['./planning-admin.component.css']
})
export class PlanningAdminComponent implements OnInit {

  planifications: Planification[] = []
  selectedPlanification: Planification = null
  active = false
  premium = false
  loading: boolean = false
  @ViewChild('downloadPDFLink') private downloadPDFLink: ElementRef;

  constructor(private planificationService: PlanificationService) { }

  ngOnInit() {

    this.cargarPlanificaciones()
    

  }

  cargarPlanificaciones(){
    this.planificationService.getPlanificationsAdmin().subscribe(
      (val) => {
        this.planifications = val.planifications
      },
      err => {

      },
      () => {

      }
    )
    
  }

  actualizar(event, plan) {
    this.loading = true;
     
        this.planificationService.updatePlanification(this.selectedPlanification).subscribe(
          (val) => {
            alert('Carga exitosa');
            this.cargarPlanificaciones()
            this.selectedPlanification = null
          },
          err => {
          },
          () => {
    
            this.loading = false;
          }
        )
    /*
    setTimeout(() => {
      console.log(this.selectedPlanification);
      alert('done!');
      this.loading = false;
    }, 1000);
*/

  }

  cancelar(event, plan) {
    this.loading = true;
     
        this.planificationService.removePlanification(this.selectedPlanification).subscribe(
          (val) => {
            alert('Se ha cancelado la planificación');
            this.cargarPlanificaciones()
            this.selectedPlanification = null
          },
          err => {
           
          },
          () => {
    
            this.loading = false;
          }
        )
    /*
    setTimeout(() => {
      console.log(this.selectedPlanification);
      alert('done!');
      this.loading = false;
    }, 1000);
*/

  }

  onChangeObj(newObj) {
    this.selectedPlanification = newObj;
    this.selectedPlanification.premium == '1' ? this.premium = true : this.premium = false
    this.selectedPlanification.active == '1' ? this.active = true : this.active = false
    //console.log(this.selectedPlanification)
    // ... do other stuff here ...
  }


  onPremiumChange(eve: any) {
    this.premium = !this.premium;
    this.premium == true ? this.selectedPlanification.premium = '1' : this.selectedPlanification.premium = '0'
  }

  onActiveChange(eve: any) {
    this.active = !this.active;
    this.active == true ? this.selectedPlanification.active = '1' : this.selectedPlanification.active = '0'
  }

  updatePlanification() {
    this.loading = true;
    // In a real-world app you'd have a http request / service call here like
    // this.http.post('apiUrl', formModel)

    this.planificationService.uploadPlanification(this.selectedPlanification).subscribe(

      (val) => {
        alert('Carga exitosa');
        this.cargarPlanificaciones()
      },
      err => {
        
      },
      () => {

        this.loading = false;
      }
    )
    /*
     setTimeout(() => {
       console.log(this.selectedPlanification);
       alert('done!');
       this.loading = false;
     }, 1000);
      */
  }

  descargarArchivo() {
    this.planificationService.downloadPDF(this.selectedPlanification.url).subscribe(
      (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
      }
    );

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceLoadComponent } from './resource-load.component';

describe('ResourceLoadComponent', () => {
  let component: ResourceLoadComponent;
  let fixture: ComponentFixture<ResourceLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ResourcesService } from '../../servicios/resources.service';

@Component({
  selector: 'app-resource-load',
  templateUrl: './resource-load.component.html',
  styleUrls: ['./resource-load.component.css']
})
export class ResourceLoadComponent implements OnInit {

  form: FormGroup;
  loading: boolean = false;
  resourcesCategory: {
    name:string,
    id_resource:string
  } []

  selectedLevel: string

  @ViewChild('filePDF') filePDF: ElementRef;

  constructor(private fb: FormBuilder,
    private resourceService: ResourcesService) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      pdf: null,
      id_category_resource : ['', Validators.required]

           
    });
  }

  onSubmit() {
    
    
    const formModel = this.form.value;
    
    this.loading = true;
    // In a real-world app you'd have a http request / service call here like
    // this.http.post('apiUrl', formModel)
       
    this.resourceService.uploadResource(formModel).subscribe(
      (val) => {
        alert('Carga exitosa');
        console.log(val)
      },
      err => {
        console.log('Error!');
        console.log(err)
      },
      () => {
        
        this.loading = false;
      }
    )
   /*
    setTimeout(() => {
      console.log(formModel);
      alert('done!');
      this.loading = false;
    }, 1000);
  */
  }

  onFileChangePDF(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if(file.type == "application/pdf"){
          this.form.get('pdf').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          })
        }
        else{
          alert("Archivo no valido")
          this.clearFilePDF()
        }
      };
    }
  }

  clearFilePDF() {
    this.form.get('pdf').setValue(null);
    this.filePDF.nativeElement.value = '';
  }

  ngOnInit() {
    this.resourceService.getResourceCategory().subscribe(
      (val) => {
        this.resourcesCategory = val.resources
        this.selectedLevel = this.resourcesCategory[0].id_resource;
      
      },
      err => {

      },
      () => {

      }
    )
  }

}

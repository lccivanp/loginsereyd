import { Component, OnInit } from '@angular/core';
import { AuthAdminService } from '../../servicios/auth-admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private auth: AuthAdminService ,private router: Router) { }

  ngOnInit() {
    
    
    if (this.auth.isLoggedIn) {
     
      //console.log(this.auth.userId)
      //this.message = "Hola "+this.auth.userId.name + " !!";
    } else {
      //this.router.navigate(['admin'])
      this.router.navigate(['loginAdmin'])

    }
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthAdminService } from '../../servicios/auth-admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {

  constructor(private auth: AuthAdminService,
    private router: Router) { }

  ngOnInit() {
    console.log(this.auth)
    if (this.auth.isLoggedIn) {
      this.router.navigate(['admin'])

      console.log(this.auth.userId)
      //this.message = "Hola "+this.auth.userId.name + " !!";
    } else {
      //this.router.navigate(['admin'])
    }

  }

  loginUserAdmin(event) {
    event.preventDefault();
    const target = event.target
    const username = target.querySelector('#username').value
    const password = target.querySelector('#password').value
    this.auth.getUserDetails(username, password).subscribe(
      (val) => {
        console.log("POST call successful value returned in body", val.user);
        this.router.navigate(['admin'])
        this.auth.setLoggedIn(true)
        this.auth.setIdUser(val.user)
      },
      err => {
        console.log("POST call in error", err);
        window.alert("No se encontraro usuario")
      },
      () => {
        console.log("The POST observable is now completed.");
      })

  }

}

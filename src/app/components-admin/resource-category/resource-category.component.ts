import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../servicios/resources.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-resource-category',
  templateUrl: './resource-category.component.html',
  styleUrls: ['./resource-category.component.css']
})
export class ResourceCategoryComponent implements OnInit {

  form: FormGroup;
  resourcesCategory: {
    name:string,
    id_resource:string,
    active?:string
  } []

  resourceCateSelect : {
    name:string,
    id_resource:string,
    active?:string
  }

  active = false
  loading: boolean = false

  constructor(private fb: FormBuilder,
    private resourceService : ResourcesService) {
    this.createForm();
   }

  ngOnInit() {
    this.cargarCategorias()
  }


  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
    

           
    });
  }

  onSubmit() {
    
    
    const formModel = this.form.value;
    
    this.loading = true;
    // In a real-world app you'd have a http request / service call here like
    // this.http.post('apiUrl', formModel)
       
    this.resourceService.uploadResourceCategory(formModel).subscribe(
      (val) => {
        alert('Carga exitosa');
        this.cargarCategorias();
      },
      err => {
      },
      () => {
        
        this.loading = false;
      }
    )
   /*
    setTimeout(() => {
      console.log(formModel);
      alert('done!');
      this.loading = false;
    }, 1000);
  */
  }


  cargarCategorias(){
    this.resourceService.getResourceCategory().subscribe(
      (val) => {
        this.resourcesCategory = val.resources
        //console.log(val)
      },
      err => {

      },
      () => {

      }
    )
  }

  onChangeCateg(newObj) {
    this.resourceCateSelect = newObj
    
    this.resourceCateSelect.active == '1' ? this.active = true : this.active = false
    /*
    this.selectedPlanification = newObj;
    this.selectedPlanification.premium == '1' ? this.premium = true : this.premium = false
    this.selectedPlanification.active == '1' ? this.active = true : this.active = false
    */
    // ... do other stuff here ...
  }

  onActiveChange(eve: any) {
    this.active = !this.active;
    this.active == true ? this.resourceCateSelect.active = '1' : this.resourceCateSelect.active = '0'
  }


  actualizarCategoria(event, plan) {
    this.loading = true;
  
        this.resourceService.updateResourceCategory(this.resourceCateSelect).subscribe(
          (val) => {
            alert('Se actualizo');
            this.cargarCategorias()
            this.resourceCateSelect = undefined
          },
          err => {
          },
          () => {
    
            this.loading = false;
          }
        )
   
     /* 
    setTimeout(() => {
      console.log(this.resourceCateSelect);
      alert('done!');
      this.loading = false;
    }, 1000);
 */

  }

  cancelarCategoria(event, plan) {
    this.loading = true;
   
        this.resourceService.removeResourceCategory(this.resourceCateSelect).subscribe(
          (val) => {
            alert('Se ha cancelado la categoria');
            this.cargarCategorias()
            this.resourceCateSelect = undefined
          },
          err => {
           
          },
          () => {
    
            this.loading = false;
          }
        )
    /*
    setTimeout(() => {
      console.log(this.resourceCateSelect);
      alert('done!');
      this.loading = false;
    }, 1000);
    */


  }

}

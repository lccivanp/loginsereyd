import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../../servicios/resources.service';
import { Resource } from '../../modelos/resource';

@Component({
  selector: 'app-resource-admin',
  templateUrl: './resource-admin.component.html',
  styleUrls: ['./resource-admin.component.css']
})
export class ResourceAdminComponent implements OnInit {

  resourcesCategory: {
    name:string,
    id_resource:string
  } []
  resourceCateSelect : {
    name:string,
    id_resource:string
  }

  resourceList :Resource[]
  resourceSelect : Resource = null

  active = false
  loading: boolean = false
  

  constructor(private resourceService : ResourcesService) { }

  ngOnInit() {
    this.cargarCategorias()

  }

  cargarCategorias(){
    this.resourceService.getResourceCategory().subscribe(
      (val) => {
        this.resourcesCategory = val.resources
        //console.log(val)
      },
      err => {

      },
      () => {

      }
    )
  }

  onChangeCateg(newObj) {
    //console.log(newObj);
    this.resourceCateSelect = newObj
    this.resourceService.getResourceByCategory(this.resourceCateSelect.id_resource).subscribe(
      (val) => {
        this.resourceList = val.resources
       // console.log(val)
      },
      err => {

      },
      () => {

      }
    )
    
    /*
    this.selectedPlanification = newObj;
    this.selectedPlanification.premium == '1' ? this.premium = true : this.premium = false
    this.selectedPlanification.active == '1' ? this.active = true : this.active = false
    */
    // ... do other stuff here ...
  }

   onChangeReso(newObj) {
    //console.log(newObj);
    this.resourceSelect = newObj
    
    //this.selectedPlanification.premium == '1' ? this.premium = true : this.premium = false
    this.resourceSelect.active == '1' ? this.active = true : this.active = false
    
  }

  actualizar(event, plan) {
    this.loading = true;
     
        this.resourceService.updateResource(this.resourceSelect).subscribe(
          (val) => {
            alert('Se actualizo');
            this.cargarCategorias()
            this.resourceSelect = null
          },
          err => {
          },
          () => {
    
            this.loading = false;
          }
        )
    /*
    setTimeout(() => {
      console.log(this.selectedPlanification);
      alert('done!');
      this.loading = false;
    }, 1000);
*/

  }

  onActiveChange(eve: any) {
    this.active = !this.active;
    this.active == true ? this.resourceSelect.active = '1' : this.resourceSelect.active = '0'
  }


  updatePlanification() {
    this.loading = true;
    // In a real-world app you'd have a http request / service call here like
    // this.http.post('apiUrl', formModel)

    this.resourceService.uploadResource(this.resourceSelect).subscribe(

      (val) => {
        alert('Carga exitosa');
        this.cargarCategorias()
        this.resourceList = undefined
      },
      err => {
        
      },
      () => {

        this.loading = false;
      }
    )
    /*
     setTimeout(() => {
       console.log(this.selectedPlanification);
       alert('done!');
       this.loading = false;
     }, 1000);
      */
  }

  cancelar(event, plan) {
    this.loading = true;
     
        this.resourceService.removeResource(this.resourceSelect).subscribe(
          (val) => {
            alert('Se ha cancelado el recurso');
            this.cargarCategorias()
            this.resourceSelect = null
          },
          err => {
           
          },
          () => {
    
            this.loading = false;
          }
        )
    /*
    setTimeout(() => {
      console.log(this.selectedPlanification);
      alert('done!');
      this.loading = false;
    }, 1000);
*/

  }

  descargarArchivo() {
    this.resourceService.downloadPDF(this.resourceSelect.url).subscribe(
      (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
      }
    );

  }

}

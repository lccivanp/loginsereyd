import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceAdminComponent } from './resource-admin.component';

describe('ResourceAdminComponent', () => {
  let component: ResourceAdminComponent;
  let fixture: ComponentFixture<ResourceAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

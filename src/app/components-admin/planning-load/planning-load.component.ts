import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { PlanificationService } from '../../servicios/planification.service';

@Component({
  selector: 'app-planning-load',
  templateUrl: './planning-load.component.html',
  styleUrls: ['./planning-load.component.css']
})
export class PlanningLoadComponent implements OnInit {

  form: FormGroup;
  loading: boolean = false;

  @ViewChild('filePDF') filePDF: ElementRef;
  @ViewChild('fileImagen') fileImage: ElementRef;
  @ViewChild('fileWord') fileWord: ElementRef;
  @ViewChild('filePreview') filePreview: ElementRef;

  constructor(private fb: FormBuilder,
    private planificationService: PlanificationService) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      formative_field:null,
      imagen: null,
      pdf: null,
      word:null,
      preview:null,
      premium: null      
    });
  }

  onChange(isChecked: boolean) {
    if(isChecked) {
      this.form.get('premium').setValue(
       1
      )
    } else {
      this.form.get('premium').setValue(
        0
      )
    }
    
  }

  onFileChange(event, value: string) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        
        this.form.get(value).setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })

      };
    }
  }

  
  onFileChangePDF(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if(file.type == "application/pdf"){
          this.form.get('pdf').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          })
        }
        else{
          alert("Archivo no valido")
          this.clearFilePDF()
        }
      };
    }
  }

  onFileChangeImage(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        var test = file.type;
        if( test.indexOf('image') >= 0){
          this.form.get('imagen').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          })
        }
        
        else{
          alert("Archivo no valido")
          this.clearImage()
        }
      }
    }
  }
  

  onFileChangeWord(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        var test = file.type;
        if( test.indexOf('application/vnd.openxmlformats-officedocument') >= 0){
          this.form.get('word').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          })
        }
        
        else{
          alert("Archivo no valido")
          this.clearFileWord()
        }
      }
    }
  }

  onFileChangePreview(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if(file.type == "application/pdf"){
          this.form.get('preview').setValue({
            filename: file.name,
            filetype: file.type,
            value: reader.result.split(',')[1]
          })
        }
        else{
          alert("Archivo no valido")
          this.clearFilePreview()
        }
      };
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
    input.append('name', this.form.get('name').value);
    input.append('pdf', this.form.get('pdf').value);
    input.append('image', this.form.get('image').value);
    return input;
  }

  onSubmit() {
    
    
    const formModel = this.form.value;
    
    this.loading = true;
    // In a real-world app you'd have a http request / service call here like
    // this.http.post('apiUrl', formModel)
    
    this.planificationService.uploadPlanification(formModel).subscribe(
      (val) => {
        alert('Carga exitosa');
        console.log(val)
      },
      err => {
        console.log('Error!');
        console.log(err)
      },
      () => {
        
        this.loading = false;
      }
    )
   /*
    setTimeout(() => {
      console.log(formModel);
      alert('done!');
      this.loading = false;
    }, 1000);
     */
  }

  clearFile(value: string) {
    this.form.get(value).setValue(null);
    
  }

  clearFilePDF() {
    this.form.get('pdf').setValue(null);
    this.filePDF.nativeElement.value = '';
  }

  clearImage() {
    this.form.get('imagen').setValue(null);
    this.fileImage.nativeElement.value = '';
  }

  clearFilePreview() {
    this.form.get('preview').setValue(null);
    this.filePreview.nativeElement.value = '';
  }

  clearFileWord() {
    this.form.get('word').setValue(null);
    this.fileWord.nativeElement.value = '';
  }

  ngOnInit() {
  }

}

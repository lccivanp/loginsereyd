import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningLoadComponent } from './planning-load.component';

describe('PlanningLoadComponent', () => {
  let component: PlanningLoadComponent;
  let fixture: ComponentFixture<PlanningLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanningLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

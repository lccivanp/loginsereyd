import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthAdminService } from '../../servicios/auth-admin.service';

@Component({
  selector: 'app-logout-admin',
  templateUrl: './logout-admin.component.html',
  styleUrls: ['./logout-admin.component.css']
})
export class LogoutAdminComponent implements OnInit {

  constructor(
    private router : Router,
    private auth: AuthAdminService) { }

  ngOnInit() {
    this.auth.deleteSessionUser()
  }

}

export class Notice {
    id_notice: string;
    name: string;
    description: string;
    date: string;
    time: string;
    id_group: string;
}
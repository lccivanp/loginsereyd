import { Student } from "./student";
import { Homework } from "./homework";
import { Cooperation } from "./cooperation";
import { Notice } from "./notice";
import { Group } from "./group";

export class Groups{
    group : Group
    cooperations : Cooperation[]
    homeworks : Homework[]
    notices : Notice[]
    students :Student[]
    active? : string
}
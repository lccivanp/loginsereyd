export class Student{
    id_student :  string ;
    name : string ;
    lastname :  string ;
    birthday :  string ;
    gender :  string ;
    image :  string;
    emergency_contact :  string ;
    id_group :  string ;
    email? : string;
    password? : string;
}
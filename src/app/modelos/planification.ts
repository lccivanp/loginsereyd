export class Planification{
    id_planification: string;
    url: string;
    name: string;
    description: string ;
    image: string;
    url_word: string;
    preview: string;
    formative_field: string;
    premium: string;
    id_user: string;
    active?: string;
    conteo?: string;
}
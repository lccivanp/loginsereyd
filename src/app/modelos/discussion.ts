import { Answer } from "./answer";

export class Discussion{
    id_discussion: string;
    title: string;
    question: string;
    date_creation: string;
    ranking: string;
    id_user: string;
    name: string;
    image: string;
    like_count: string;
    answers : Answer[];
    like :string;
}
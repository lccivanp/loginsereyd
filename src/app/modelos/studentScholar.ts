export class studentScholar {
    error: string;
    attendance: {
        count: string
    };
    behavior: {
        description: string;
        average: string
    };
    language: number;
    math: number;
    world: number;
    health: number;
    social: number;
    art: number;
    diagnostic: number;
    school_performance: number;

    getLanguage(): number{
        return this.language
    }

    getMath():number{
        return this.math
    }

    
    getWorld(): number{
        return this.world
    }

    getHealth(): number{
        return this.health;
    }
    
    getSocial(): number{
        return this.social
    }

    getArt(): number{
        return this.art
    }
}
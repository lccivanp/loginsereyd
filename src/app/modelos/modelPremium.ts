export class modelPremium{
    userExists  :	number ;
    error	    :   number;
    description :	string;
    apikey?     :	string;
    code?       :	string;
    annual?     :	string;
    init_date?  :	string;
    finish_date	?:	string;
    planification_count	?:	string;
    resource_count	?:	string;
    info_pay ?:{
        error:	number;
        cancelled:	number;
        desc:	string;
        status:	string;
        card_number?:	string;
        holder_name?:	string;
        charge_date?:	string;
        accion?:	string;
        amount?:	number

        }
}